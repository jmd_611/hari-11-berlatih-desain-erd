#ERD Challenge
 
Kalian ditantang untuk membuat desain ERD dari sebuah aplikasi Review Film Berikut ini penjelasan tentang aplikasi tersebut

##Aplikasi Review Film
 
###Description: 

Sebuah Website Review Film seperti Metacritic/IMDB/Rotten Tomatoes dimana setiap user dapat memberi rating pada setiap film dan dapat melihat informasi tentang film tersebut

###Ketentuan Website Review Film:

Setiap user hanya dapat memliki satu profil <br/>
Setiap genre memiliki banyak film<br/>
Setiap user dapat memberikan kritik beserta rating dalam setiap film, dan setiap film dapat dikritik dan diberi rating oleh banyak user<br/>
Setiap cast dapat memiliki peran dalam setiap film, dan film juga dapat memiliki banyak cast beserta peran<br/>
 

###Requirement :
1. Data di table profil : id, umur, bio, alamat <br/>
2. Data di table user: id, name, email, password <br/>
3. Data di table cast : id, nama, umur, bio <br/>
4. Data di table film : id, judul, rigkasan, tahun, poster <br/>
5. Data di table genre: id, nama <br/>
 

Output dari Tugas hari ini yaitu gambar dari desain ERD (boleh berupa gambar PNG atau JPEG). Simpan gambar tersebut ke dalam repositori di Gitlab (dengan git push bukan upload manual di gitlab) lalu kirimkan link gitlab gambar tersebut ke dasbor member.jabarcodingcamp.id.